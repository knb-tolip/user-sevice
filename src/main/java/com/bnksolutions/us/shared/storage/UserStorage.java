package com.bnksolutions.us.shared.storage;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import com.bnksolutions.us.shared.dto.User;

@Component
public class UserStorage {

	private Map<String, User> database = new HashMap<>();

	@PostConstruct
	public void init() {
		User u = null;
		for (int i = 10; i < 30; i++) {
			u = new User();
			u.setEmail("user0" + i + "@knb-tolip.com");
			u.setId(String.valueOf(i));
			u.setName("user " + i);
			database.put("" + i, u);
		}
	}

	public User getUserById(String id) {
		return this.database.get(id);
	}
}
