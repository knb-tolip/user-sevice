package com.bnksolutions.us.shared.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDTO {
	private String orderId;
	private String orderCode;
	private String orderName;
	private String orderStatus;
}