package com.bnksolutions.us.shared.request;
import lombok.Data;

@Data
public class OrderRequest {
	private String userId;
}