package com.bnksolutions.us.shared.response;

import com.bnksolutions.us.shared.dto.User;

import lombok.Data;

@Data
public class UserResponse {
	private User user;
	private OrderResponse orders;
}
