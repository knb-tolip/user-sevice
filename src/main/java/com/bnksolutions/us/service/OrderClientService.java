package com.bnksolutions.us.service;

import com.bnksolutions.us.shared.response.OrderResponse;

public interface OrderClientService {
	OrderResponse getUserOrders(String userId);
}
