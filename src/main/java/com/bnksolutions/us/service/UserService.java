package com.bnksolutions.us.service;

import com.bnksolutions.us.shared.response.UserResponse;

public interface UserService {
	UserResponse getUser(String id);
}
