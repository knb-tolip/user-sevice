package com.bnksolutions.us.service.impl;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bnksolutions.us.configuration.OrderServiceClientProperties;
import com.bnksolutions.us.service.OrderClientService;
import com.bnksolutions.us.shared.response.OrderResponse;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OrderClientServiceImpl implements OrderClientService {

	@Autowired
	@Qualifier("apiGatewayRestClient")
	private RestTemplate restCli;

	@Autowired
	private OrderServiceClientProperties orderProps;

	@Override
	public OrderResponse getUserOrders(String userId) {
		try {
			
			String path = orderProps.getUserOrderPath().concat("{userId}");			
			ResponseEntity<OrderResponse> res = restCli.getForEntity(path,
					OrderResponse.class, userId);
			return res.getBody();
		}catch(Exception ex) {
			log.error("Got error when request to order service");
			log.error(ex.getMessage());
			log.debug("Full error :::::{}", ex);
			return null;
		}
	}

}
