package com.bnksolutions.us.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bnksolutions.us.repository.UserRepository;
import com.bnksolutions.us.service.OrderClientService;
import com.bnksolutions.us.service.UserService;
import com.bnksolutions.us.shared.dto.User;
import com.bnksolutions.us.shared.response.UserResponse;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private OrderClientService orderService;

	@Override
	public UserResponse getUser(String id) {
		User user = userRepo.getUserById(id);
		UserResponse rs = new UserResponse();
		rs.setUser(user);
		rs.setOrders(orderService.getUserOrders(id));
		return rs;
	}

}
