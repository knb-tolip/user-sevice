package com.bnksolutions.us.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bnksolutions.us.shared.dto.User;
import com.bnksolutions.us.shared.storage.UserStorage;

@Component
public class UserRepository {
	@Autowired
	private UserStorage storage;

	public User getUserById(String id) {
		return storage.getUserById(id);
	}
}
