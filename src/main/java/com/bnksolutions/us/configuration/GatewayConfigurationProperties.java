package com.bnksolutions.us.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "internal.gateway")
@Data
public class GatewayConfigurationProperties {
	private String baseUrl;
	
}
