package com.bnksolutions.us.configuration;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestClientConfiguration {
	@Autowired
	private GatewayConfigurationProperties props;

	@Bean(name = "apiGatewayRestClient")
	public RestTemplate configureRestTemplate(RestTemplateBuilder builder) {
		RestTemplate template = builder.rootUri(props.getBaseUrl())
				.messageConverters(new MappingJackson2HttpMessageConverter())
				.additionalInterceptors(clientHttpRequestInterceptor()).build();

		return template;
	}

	private ClientHttpRequestInterceptor clientHttpRequestInterceptor() {
		ClientHttpRequestInterceptor res = new ClientHttpRequestInterceptor() {

			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
					throws IOException {
				request.getHeaders().setContentType(MediaType.APPLICATION_JSON);
				return execution.execute(request, body);
			}
		};
		return res;
	}

}
