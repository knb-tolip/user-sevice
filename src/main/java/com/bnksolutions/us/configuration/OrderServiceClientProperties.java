package com.bnksolutions.us.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "order-service")
@Data
public class OrderServiceClientProperties {
	
	private String userOrderPath;

}
