package com.bnksolutions.us.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bnksolutions.us.service.UserService;
import com.bnksolutions.us.shared.response.UserResponse;

@RestController
public class UserController {
	@Autowired
	private UserService userService;

	@GetMapping("/me")
	public UserResponse getUser(@RequestHeader("x-user-id") String id) {
		return userService.getUser(id);
	}

}
